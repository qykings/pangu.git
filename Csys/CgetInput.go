package Csys

/**
获取命令行交互数据
qykings
 */
import (
	"bufio"
	"fmt"
	"os"
)

func CgetIntput() string {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		line := scanner.Text()

		fmt.Println("close	program  input exit ")
		fmt.Println("input txt:")
		fmt.Println(line) // Println will add back the final '\n'

		if line == "exit" {
			os.Exit(0)
		}

		return line

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}

	return ""
}
