package clog

import (
	"log"
	"os"
	"fmt"
	"time"
)

type CLogTxt struct {
	LogfileName string
}

func (c *CLogTxt) Init(logfilename string,prefix string) {

	c.LogfileName = logfilename

	logFile, err := os.OpenFile(c.LogfileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
	if err != nil {
		panic(err)
	}
	log.SetOutput(logFile) // 将文件设置为log输出的文件
	log.SetPrefix(prefix)
	log.SetFlags(log.Lshortfile | log.LUTC)

}

func (c *CLogTxt)AddLog(prefix string,msg string)  {
	now := time.Now()
	date := now.Format("2006-01-02 15:04:05")

	result := date+ "|" + prefix+"|"+  msg

	fmt.Println(result)

	log.Println(result) // log 还是可以作为输出的前缀
}