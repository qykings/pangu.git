package util

import "encoding/hex"

// 字节转为十六进制字符串
func ByteTo16String(by []byte) string {
	return hex.EncodeToString(by)
}
