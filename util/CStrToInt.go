package util

/**
字符串转换
qykings
 */
import (
	"fmt"
	"strconv"
	)

func CStrToInt(str string) int64 {
	tmp, err := strconv.ParseInt(str, 10, 32)

	if err != nil {
		fmt.Print(err.Error())
	}
	return tmp
}
