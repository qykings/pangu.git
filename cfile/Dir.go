package cfile

import (
  "fmt"
	"os"
	"path/filepath"
)

/***
qykings
文件目录操作
 */

type Dir struct {
}


func PathExists(path string) bool {
    _, err := os.Stat(path)
    if err == nil {
        return true 
    }
    if os.IsNotExist(err) {
        return false 
    }
    return false
}

/**
删除指定的文件
*/

func RemoveFile(fileurl string) bool {
	err := os.Remove(fileurl)

	if err != nil {
		// 删除失败
		return false
	}
	// 删除成功

	return true
}

/**
删除文件夹及其包含的所有子目录和所有文件
*/
func RemoveDir(url string) bool {
	err := os.RemoveAll(url)

	if err != nil {
		// 删除失败
		return false
	}
	// 删除成功

	return true
}


/**
创建文件夹
**/
func   Mkdir( url string ) bool {
  if PathExists( url )==true{
   return true
 }

  err := os.MkdirAll(url, os.ModePerm)
        if err != nil {
            fmt.Printf("mkdir failed![%v]\n", err)
            return false
        } else {
            fmt.Printf("mkdir success!\n")
        }


   return true
}


/**文件夹下的文件列表**/
func DirFileList(dirpath string) ([]string, error) {
	var dir_list []string
	var filelist []string
	dir_err := filepath.Walk(dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err
			}
			if f.IsDir() {
				dir_list = append(dir_list, path)
				return nil
			} else {
				filelist = append(filelist, path)
				return nil
			}

		})
	return filelist, dir_err
}


/**文件夹**/
func FolderList(dirpath string) ([]string, error) {
	var dir_list []string

	dir_err := filepath.Walk(dirpath,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err
			}
			if f.IsDir() {
				dir_list = append(dir_list, path)
			}

			return nil

		})
	return dir_list, dir_err
}