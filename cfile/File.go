package cfile

import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"

	"os"
	"strings"
)

/***
qykings
文件读写操作
*/

type File struct {
}

func (s *File) Version() {
	fmt.Println("kfile version is 0.1")
}

/**
* 判断文件是否存在
  存在返回 true
  不存在返回false
*/
func (s *File)IsExist(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true

}

/**
mode 文件写入操作类型的
这个有问题，不能增加文本，后面追加文件 有问题
*/
func (s *File) WriteFileMode(filePath string, content string, mode fs.FileMode) {
	var f *os.File
	var tmpErr error

	if s.IsExist(filePath) {
		//如果文件存在
		f, tmpErr = os.OpenFile(filePath, os.O_APPEND, mode) //打开文件
		fmt.Println("文件存在")
	} else {
		f, tmpErr = os.Create(filePath) //创建文件
		//fmt.Println("文件不存在")
	}

	if tmpErr != nil {
		fmt.Println(tmpErr)
		return
	}

	if mode == fs.ModeAppend {

		// 字节方式写入
		//_, err := f.WriteAt([]byte(content))//Write([]byte(content))
		//if err != nil {
		//	println("字节方式写入File write error",err)
		//	return
		//}


	}else{
		io.WriteString(f, content) //写入文件(字符串)
	}


	f.Close()
}

func (s*File) AppendFile(filename string,content string)  {

	fp, _ := os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModeAppend|os.ModePerm) // 读写方式打开

	// defer延迟调用
	defer fp.Close() //关闭文件，释放资源。

	fp.WriteString(content)
	//count, _ := fp.WriteString(content)
	//fmt.Println(count)                                // 31(字节，一个汉字3个字节)

	//// 写入文件（字符切片） Write()
	//count, _ = fp.Write([]byte("中文信息ooo")) // 强制类型转换 string-->[]byte
	//fmt.Println(count)

}


/**
没有文件路径会自动创建
 */
func (s*File) CreatUrl( url string)  {
 s.checkUrl(url)
}

func (s *File)checkUrl( targeturl string)  {
	result := strings.LastIndex(targeturl, "/")
	var filePath string
	if result > -1 {
		filePath = targeturl[0:result]
		if !s.IsExist(filePath) {
			os.MkdirAll(filePath, os.ModePerm)
		}
	}
}

/**
没有文件及路径会自动创建
*flag os.O_RDONLY|os.O_CREATE|os.O_APPEND
*/
func (s *File) WriteFile(targeturl string, param string,flag int) {

	 s.checkUrl(targeturl)

	data := []byte(param)
	error:= ioutil.WriteFile(targeturl, data, fs.ModePerm)
	if error!= nil {
		fmt.Println("写入文件失败:", targeturl,error.Error())
	}





}

func (s *File) ReadFile(url string) string {
	data, err := ioutil.ReadFile(url)
	if err != nil {
		fmt.Println("File reading error", err)
		return ""
	}

	return string(data)
}



/**
编写一个函数，接收两个文件路径:
 */
func (s*File)CopyFile(dstFileName string, srcFileName string) (written int64, err error) {
	s.checkUrl(dstFileName)

	srcFile, err := os.Open(srcFileName)
	if err != nil {
		fmt.Printf("open file error = %v\n", err)
	}
	defer srcFile.Close()

	//通过srcFile，获取到READER
	reader := bufio.NewReader(srcFile)

	//打开dstFileName
	dstFile, err := os.OpenFile(dstFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("open file error = %v\n", err)
		return
	}

	//通过dstFile，获取到WRITER
	writer := bufio.NewWriter(dstFile)


	defer dstFile.Close()

	return io.Copy(writer, reader)
}


