package cimg

import (
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"os"
	"fmt"
	"strings"
	"path/filepath"
)

/***
qykings
图片文件读写操作
*/

type CIMG struct {
}


func (s *CIMG) LoadImage(path string) (img image.Image, err error) {
	file, err := os.Open(path)
	if err != nil {
		return nil ,err
	}
	defer file.Close()
	img, _, err = image.Decode(file)

	if err != nil {
		println("loadImage is erro")
		fmt.Println(err)

	}

	return img, err
}

func (s *CIMG) SaveImage(p string, src image.Image, quality int) error {
	f, err := os.OpenFile(p, os.O_SYNC|os.O_RDWR|os.O_CREATE, 0666)

	if err != nil {
		return err
	}



	defer f.Close()
	ext := filepath.Ext(p)

	if strings.EqualFold(ext, ".jpg") || strings.EqualFold(ext, ".jpeg") {

		err = jpeg.Encode(f, src, &jpeg.Options{Quality: quality})

	} else if strings.EqualFold(ext, ".png") {
		err = png.Encode(f, src)
	} else if strings.EqualFold(ext, ".gif") {
		err = gif.Encode(f, src, &gif.Options{NumColors: 256})
	}
	return err
}




func  (s *CIMG) ImageCopy(src image.Image, x, y, w, h int) image.Image {

	var subImg image.Image

	if rgbImg, ok := src.(*image.YCbCr); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.YCbCr) //图片裁剪x0 y0 x1 y1
	} else if rgbImg, ok := src.(*image.RGBA); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.RGBA) //图片裁剪x0 y0 x1 y1
	} else if rgbImg, ok := src.(*image.NRGBA); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.NRGBA) //图片裁剪x0 y0 x1 y1
	}

	return subImg
}